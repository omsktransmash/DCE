class BaseInfantry:

    def __init__(self):
        # order of elements in the list almost always matters

        self.origin = ['bionic']
        self.sensors = [
            # human eyes
            # human ears
            # human nose
        ]
        self.weapons = [
            # fist
        ]
        self.armor = [
            # cloth armor
        ]
        self.tactics = [  ## include ROE?
            # attack only if attacked
            # use best weapon first
            # do not conserve ammo
            # move and attack
        ]
