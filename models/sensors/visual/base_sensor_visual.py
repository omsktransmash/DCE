class BaseSensorVisual:

    def __init__(self):
        # order of elements in the list almost always matters

        self.origin = []
        self.hp = 0
        self.df = 0
        self.range = 0  # meters
        self.los = 0  # 0: direct 1: em wave, 2: sound wave?
        self.los_cone = 0.0
